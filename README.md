# QuikLinks RESTful Connector API

This repository contains the specification for the QuikLinks RESTful connector API.

## Overview

* Implementation language-independent (OpenAPI 3.0)
* Distributed/Remote
* Supports asynchronous operation